package com.analyze.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.analyze.qa.testbase.TestBase;

public class HomePage extends TestBase{

	//Page Factory
	
	@FindBy(xpath="//a[contains(text(),'Dashboard')]")
	WebElement dashboardOption;
	
	@FindBy(xpath="//a[contains(text(),'Custom Reports')]")
	WebElement customReportsOption;
	
	@FindBy(xpath="//li[@class='menuAdmin ng-scope']")
	WebElement adminOption;
	
	@FindBy(xpath="//a[contains(text(),'My Jobs')]")
	WebElement myJobsOption;
	
	// initialize the elements - create a constructor
	
	public HomePage() {
		
		PageFactory.initElements(driver, this);
	}
	
	//Actions
	
	public boolean verifyDashboardOption (){
		return dashboardOption.isDisplayed();
        
	}
	
	public boolean verifyCustomReportOption (){
		return customReportsOption.isDisplayed();
	}
	
	public boolean verifyAdminOption (){
		return adminOption.isDisplayed();
	}
	
	public boolean verifyMyJobsOption (){
		return myJobsOption.isDisplayed();
	}
	
	public DashboardPage clickOnDashboard() {
		dashboardOption.click();
		return new DashboardPage();
	}
	
	public CustomReportPage clickOnCustomReport() {
		customReportsOption.click();
		return new CustomReportPage();
	}
	public AdminPage clickOnAdmin() {
		adminOption.click();
		return new AdminPage();
	}
	
	public MyJobsPage clickOnMyJobs() {
		myJobsOption.click();
		return new MyJobsPage();
	}
	
}
