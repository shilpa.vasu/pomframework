package com.analyze.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.analyze.qa.testbase.TestBase;

public class LoginPage extends TestBase{
	
	//Page Factory
	
	@FindBy(name="userName")
	WebElement username;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(xpath="//input[@type='submit']")
	WebElement signIn;
	
	
	// initialize the elements - create a constructor
	
	public LoginPage(){
		
		PageFactory.initElements(driver,this);
	}

	//Actions
	
	public HomePage login(String un,String pwd) {
		username.sendKeys(un);
		password.sendKeys(pwd);
		signIn.click();
		return new HomePage();
	}
}
