package com.smoketestcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.analyze.qa.pages.HomePage;
import com.analyze.qa.pages.LoginPage;
import com.analyze.qa.testbase.TestBase;

public class LoginTest extends TestBase{
	LoginPage loginPage;
	HomePage homePage;
	
	public LoginTest() {
		super(); // call super class TestBase constructor to read config properties
	}
	
	@BeforeMethod
	
	public void setUp() {
		initialization();
		// create an object of login page class
		loginPage = new LoginPage();
	}
	
	@Test(priority=1,description="Verify Login Page",retryAnalyzer = com.analyze.retryAnalyzer.CountAnalyzer.class)
	
	public void loginTest() {
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		String title = driver.getTitle();
		System.out.println(title);
		String actualTitle = "inovus Analyze";
		Assert.assertEquals(title, actualTitle);
	}

	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}
