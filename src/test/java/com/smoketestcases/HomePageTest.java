package com.smoketestcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.analyze.qa.pages.AdminPage;
import com.analyze.qa.pages.CustomReportPage;
import com.analyze.qa.pages.DashboardPage;
import com.analyze.qa.pages.HomePage;
import com.analyze.qa.pages.LoginPage;
import com.analyze.qa.pages.MyJobsPage;
import com.analyze.qa.testbase.TestBase;

public class HomePageTest extends TestBase {
	
	LoginPage loginPage;
	HomePage homePage;
	CustomReportPage customReportPage;
	AdminPage adminPage;
	MyJobsPage myJobsPage;
	
	public HomePageTest() {
		super(); // call super class TestBase constructor to read config properties
	}
	
	@BeforeMethod
	
	public void setUp() {
		initialization();
		// create an object of login page class
		loginPage = new LoginPage();
		customReportPage = new CustomReportPage();
		adminPage = new AdminPage();
		myJobsPage = new MyJobsPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
}
	
	@Test(priority=1)
	public void validateDashaboardOption() {
		Assert.assertTrue(homePage.verifyDashboardOption());
	}
	
	@Test(priority=2)
	public void validateCustomReport() {
		Assert.assertTrue(homePage.verifyCustomReportOption());
	}
	
	@Test(priority=3)
	public void validateAdminOption() {
		Assert.assertTrue(homePage.verifyAdminOption());
	}
	
	@Test(priority=4)
	public void validateMyJobsOption() {
		Assert.assertTrue(homePage.verifyMyJobsOption());
	}
	
	@Test(priority=5)
	public void validateClickOnCustomReports() {
		customReportPage =homePage.clickOnCustomReport();
	}
	
	@Test(priority=6)
	public void validateClickOnAdmin() {
		adminPage =homePage.clickOnAdmin();
	}
	
	@Test(priority=7)
	public void validateClickOnMyJobs() {
		myJobsPage =homePage.clickOnMyJobs();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.quit();
	}
}